import eu from './img/eu.png';
import './App.css';
import React from "react";

function App() {
  return (
    <div className="App">
      <header className="App-header" id="alteracor">
        <img src={eu} className="App-logo" alt="logo" />
        <p>
          Esse é o Thiago Heilmann.
        </p>
        <a
          className="App-link"
          href="https://www.youtube.com/channel/UCLmj9Y-YTTftS9vdxZhuy3Q"
          target="_blank"
          rel="noopener noreferrer"
        >
          Canal do andre
        </a>
      </header>
    </div>
  );
}

export default App;
